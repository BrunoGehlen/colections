import { Route } from 'react-router-dom'
import RickAndMorty from '../Pages/rick-and-morty'
import Pokemons from '../Pages/Pokemons'
import Favorites from '../Pages/Favorites'

const Routes = ({
    addToRickAndMortyFavorites,
    addToPokemonFavorites,
    pokemonfavoriteCharacters,
    removeFromPokemonFavorites,
    RickAndMortyfavoriteCharacters,
    removeFavoriteFromRickAndMorty
}) => {
    return (
        <>
              <Route exact path='/rick-and-morty/:page'>
              <RickAndMorty 
                addToFavorites={addToRickAndMortyFavorites} 
              />
            </Route>
            <Route exact path='/pokemons/:page'>
              <Pokemons 
                addToFavorites={addToPokemonFavorites} 
              />
            </Route>
            <Route exact path='/favorites/pokemons'>
              <Favorites 
                pokemonfavoriteCharacters={pokemonfavoriteCharacters}
                removeFromFavorites={removeFromPokemonFavorites}
              />
            </Route>
            <Route exact path='/favorites/rick-and-morty'>
              <Favorites 
                RickAndMortyfavoriteCharacters={RickAndMortyfavoriteCharacters}
                removeFromFavorites={removeFavoriteFromRickAndMorty}
             />
            </Route>
      </>
    )
}

export default Routes