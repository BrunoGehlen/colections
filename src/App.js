import { useState, useEffect } from 'react'
import './App.css'
import { Route, useHistory,useLocation } from 'react-router-dom'
import SideMenu from './Components/NavigationMenu/'
import PageControl from './Components/Pagination'
import logo from './logo.svg';
import Routes from './Routes'
const App = () => {

  const [rickAndMortyFavorites,setrickAndMortyFavorites] = useState([])
  const [pokemonFavorites,setPokemonFavorites] = useState([])

  const history = useHistory()
  const {pathname} = useLocation()
  
  const removeFavoriteFromPokemons = (pokemon) => {
    setPokemonFavorites([...pokemonFavorites.filter(poke => poke.name !== pokemon.name)])
  }

  const removeFavoriteFromRickAndMorty = (character) => {
    setrickAndMortyFavorites([...rickAndMortyFavorites.filter(char => char.id !== character.id)])
  }
  
  const addToRickAndMortyFavorites = (Char) => {
    if(!rickAndMortyFavorites.includes(Char)){
      setrickAndMortyFavorites([...rickAndMortyFavorites, Char])
      console.log(rickAndMortyFavorites)
    }
  }

  const addToPokemonFavorites = (Char) => {
    if(!pokemonFavorites.includes(Char)){
      setPokemonFavorites([...pokemonFavorites, Char])
      console.log(pokemonFavorites)
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className='Main_Header'>
          <img src={logo} className="App-logo" alt="logo" />
          <PageControl />
        </div>
          <div className='Side_Menu'>
            <SideMenu />         
          </div>
          <Routes 

            addToRickAndMortyFavorites={addToRickAndMortyFavorites}
            addToPokemonFavorites={addToPokemonFavorites}
            pokemonfavoriteCharacters={pokemonFavorites}
            removeFromPokemonFavorites={removeFavoriteFromPokemons}
            RickAndMortyfavoriteCharacters={rickAndMortyFavorites}
            removeFavoriteFromRickAndMorty={removeFavoriteFromRickAndMorty}
          
          />

        </header>
    </div>
  )
}

export default App
