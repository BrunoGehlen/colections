import { useParams,useHistory } from 'react-router-dom'
import { useEffect,useState } from 'react'
import CharacterCard from '../../Components/CharacterCard'
import { Button } from 'antd';
import { Input } from 'antd';

const Pokemons =({addToFavorites}) => {

    const [pokemonsCharacterList, setpokemonsCharacterList ] = useState([]) 
    const [currentPage,setCurrentPage] = useState(1)
    const [searchValue,setSearchValue] = useState('')
    const {page} = useParams()
    const history = useHistory()

    const getCharacterList = async () =>{
        
        const response = await fetch(`https://pokeapi.co/api/v2/pokemon?offset=${page * 20}&limit=20`)
        .then(res => res.json())
        .then(res => setpokemonsCharacterList([...res.results]))        
    }

    useEffect(() => getCharacterList(),[page])

    const nextPage = () => {
        if(currentPage < 10){
            setCurrentPage(currentPage + 1)
            history.push(`/pokemons/${currentPage}`)
        }
        return
    }

    const previousPage = () => {
        if(currentPage >= 1){
            setCurrentPage(currentPage - 1)
            history.push(`/pokemons/${currentPage}`)
        }
        return
    }

    return (
            <div>
                <h1 className='Header'>Pokemons</h1>
                <Input 
                    onChange={(e) => setSearchValue(e.target.value)} 
                    className='Search_Field' 
                    placeholder="Search"
                />
                <div className='characterList'>
                    {
                        pokemonsCharacterList
                            .filter(pokemon => pokemon.name.toLowerCase().includes(searchValue.toLowerCase()))
                            .map((pokemon,index) => 
                                <CharacterCard  
                                    key={index} 
                                    addToFavorites={addToFavorites} 
                                    Pokemon={pokemon} 
                                />)
                    }
                </div>
            </div>
        )



}

export default Pokemons