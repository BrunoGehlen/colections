import { useState, useEffect } from 'react'
import { useParams,useHistory } from 'react-router-dom'
import CharacterCard from '../../Components/CharacterCard'
import { Button } from 'antd';
import { Input } from 'antd';

const RickAndMorty = ({addToFavorites}) => {
    
    const [rickAndMortyCharacterList, setrickAndMortyCharacterList ] = useState([]) 
    const [currentPage,setCurrentPage] = useState(1)
    const [searchValue,setSearchValue] = useState('')
    const {page} = useParams()
    const history = useHistory()
    
    const getrickAndMortyCharacterList = async () =>{
      fetch(`https://rickandmortyapi.com/api/character/?page=${page}`)
      .then(res => res.json())
      .then(res => setrickAndMortyCharacterList(res.results))
    }
    
    useEffect(() => getrickAndMortyCharacterList(),[page])

    const nextPage = () => {
        if(currentPage < 34){
            setCurrentPage(currentPage + 1)
            history.push(`/rick-and-morty/${currentPage}`)
        }
        return
    }

    const previousPage = () => {
        if(currentPage >= 1){
            setCurrentPage(currentPage - 1)
            history.push(`/rick-and-morty/${currentPage}`)
        }
        return
    }

    return (
        <div>
            <h1 className='Header'>Rick And Morty</h1>
            <Input 
                    onChange={(e) => setSearchValue(e.target.value)} 
                    className='Search_Field' 
                    placeholder="Search"
                />
            <div className='characterList'>
                {
                    rickAndMortyCharacterList
                    .filter(character => character.name.toLowerCase().includes(searchValue.toLowerCase()))
                        .map((character,index) => 
                            <CharacterCard 
                                key={index} 
                                rickAndMortyCharacter={character} 
                                addToFavorites={addToFavorites}
                            />)
                }
            </div>
        </div>
    )

}

export default RickAndMorty