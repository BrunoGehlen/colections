import CharacterCard from "../../Components/CharacterCard"

const Favorites = ({RickAndMortyfavoriteCharacters,pokemonfavoriteCharacters, removeFromFavorites}) => {

    return (
        <>
            <h1 className='Favorite_Header'>Favorites</h1>
            <div className='characterList'>
                {RickAndMortyfavoriteCharacters?.map((character,index) => 
                <CharacterCard 
                    key={index}
                    rickAndMortyCharacter={character}
                    removeFromFavorites={removeFromFavorites}
                    />
                )}
                {pokemonfavoriteCharacters?.map((poke,index) => 
                    <CharacterCard 
                        key={index} 
                        Pokemon={poke}
                        removeFromFavorites={removeFromFavorites}
                    />
                )}
            </div>
        </>
    )
}
export default Favorites