import {useState} from 'react'
import 'antd/dist/antd.css';
import { Menu } from 'antd';
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom'

const { SubMenu } = Menu;

const rootSubmenuKeys = ['sub1', 'sub2', 'sub4'];

const SideMenu = () => {
  const [openKeys, setOpenKeys] = useState(['sub1']);

  const onOpenChange = keys => {
    const latestOpenKey = keys.find(key => openKeys.indexOf(key) === -1);
    if (rootSubmenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys(keys);
    } else {
      setOpenKeys(latestOpenKey ? [latestOpenKey] : []);
    }
  };
  
  return (
    <Menu mode="inline" theme="dark" openKeys={openKeys} onOpenChange={onOpenChange} style={{ width: 256 }}>
      <SubMenu key="sub1" icon={<MailOutlined />} title="Rick And Morty">
        <Menu.Item key="1"><Link className='Links' to={'/rick-and-morty/1'}>Rick And Morty</Link></Menu.Item>
        <Menu.Item key="2"><Link className='Links' to={'/favorites/rick-and-morty'}>Favorites</Link></Menu.Item>
      </SubMenu>
      <SubMenu key="sub2" icon={<AppstoreOutlined />} title="Pokemons">
        <Menu.Item key="5"><Link className='Links' to={'/pokemons/1'}>All Pokemons</Link></Menu.Item>
        <Menu.Item key="6"><Link className='Links' to={'/favorites/pokemons'}>Favorites</Link></Menu.Item>
      </SubMenu>
    </Menu>
  );
};

export default SideMenu