import { Pagination } from "antd";
import { useHistory, useLocation } from 'react-router-dom'


const PageControl = () => {

    const history = useHistory()
    const { pathname } = useLocation()

    return (
        <div className='Page_Control'>
        {pathname.includes('/pokemons/') &&
            <Pagination 
                className='Pagination'
                responsive={true} 
                onChange={(page) => history.push(`/pokemons/${page}`)} 
                defaultCurrent={1} 
                total={200}
                showSizeChanger={false} 
            />
        }
        {pathname.includes('/rick-and-morty/') &&
            <Pagination 
                className='Pagination'
                responsive={true}	
                onChange={(page) => history.push(`/rick-and-morty/${page}`)} 
                defaultCurrent={1} 
                total={340}
                showSizeChanger={false} 
            />
        }
        </div>
    )
}

export default PageControl