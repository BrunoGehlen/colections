import { useState,useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import 'antd/dist/antd.css';
import { Card,Button } from 'antd';
const { Meta } = Card;


const CharacterCard = ({rickAndMortyCharacter, Pokemon,addToFavorites,removeFromFavorites}) => {
    
    const { pathname } = useLocation()

  return (
        <div>
           {rickAndMortyCharacter &&
            <Card
              className='Card'
              hoverable
              style={{ width: 240 }}
              cover={<img alt="Character" src={rickAndMortyCharacter.image} />}
            >
              <Meta title={rickAndMortyCharacter.name} />
              <Meta description={`Race: ${rickAndMortyCharacter.species}`}/>
        
              <Meta description={`Origin: ${origin.name}`}/>
              {!pathname.includes('favorites') ? 
              <Meta 
                description={
                  <Button 
                    className='Card_button' 
                    type="primary" 
                    onClick={() => addToFavorites(rickAndMortyCharacter)}
                      >Add To Favorites
                    </Button>}
              />:
              <Meta 
              description={
                <Button danger
                  className='Card_button' 
                  type="primary" 
                  onClick={() => removeFromFavorites(rickAndMortyCharacter)}
                    >Remove From Favorites
                  </Button>}
              />}
            </Card>
            }
            {Pokemon &&
            <Card
              className='Card'
              hoverable
              style={{ width: 240 }}
              cover={<img alt="Character" src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${Pokemon.url.split("/")[Pokemon.url.split("/").length - 2]}.png`} />}
            >
              <Meta title={Pokemon.name} />
              {!pathname.includes('favorites') ? 
                <Meta 
                  description={
                    <Button 
                      className='Card_button'
                      type="primary" 
                      onClick={() => addToFavorites(Pokemon)}
                        >Add To Favorites
                    </Button>}
                />:
                <Meta 
                  description={
                    <Button danger
                      className='Card_button'
                      type="primary" 
                      onClick={() => removeFromFavorites(Pokemon)}
                        >Remove From Favorites
                    </Button>}
                />}
            </Card>
            }
        </div>
    )
}

export default CharacterCard